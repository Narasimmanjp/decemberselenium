package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_CreateLead";
		testDescription = "CreatingLead using Pages";
		authors = "Narasimman";
		category = "smoke";
		dataSheetName = "TC003_CreateLead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead(String userName,String password, String companyName, String firstName, String lastName, String phoneNumber, String title) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.enterPhoneNumber(phoneNumber)
		.enterTitle(title)
		.clickCreateLead()
		.verifyFirstName(firstName)
		.verifyCompanyName(companyName)
		
		;
		
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
}
