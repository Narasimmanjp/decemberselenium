package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_CreateLeadNegative extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_CreateLead";
		testDescription = "CreatingLead without mandatory Fields";
		authors = "Narasimman";
		category = "smoke";
		dataSheetName = "TC004_CreateLeadNegative";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead(String userName,String password, String companyName, String firstName, String lastName, String phoneNumber, String title) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.enterPhoneNumber(phoneNumber)
		.enterTitle(title)
		.clickCreateLeadFailedCase()
		.verifyErrorMessage()
		
		
		;
		
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
}
