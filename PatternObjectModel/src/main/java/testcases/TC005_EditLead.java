package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC005_EditLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC005_EditLead";
		testDescription = "Edit Existing Lead";
		authors = "Narasimman";
		category = "smoke";
		dataSheetName = "TC005_EditLead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead(String userName,String password,String findbyID,String findbyFirstName,
			String findbyLastName,String findbyCompany,String companyName,String firstName,String lastName
) throws InterruptedException {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickFindLeads()
		.enterLeadId(findbyID)
		.enterCompanyName(findbyCompany)
		.enterFirstName(findbyFirstName)
		.enterLastName(findbyLastName)
		.clickFindLead()
		.clickFirstResultingLead()
		.clickEditButton()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickUpdate()
		.verifyCompanyName(companyName)
		
		;
		
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
}
