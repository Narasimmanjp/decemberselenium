package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "viewLead_firstName_sp")
	WebElement eleFirstName;
	public ViewLeadPage verifyFirstName(String data) {		
		verifyExactText(eleFirstName, data);
		return this;
	}
	
	@FindBy(how = How.ID, using = "viewLead_companyName_sp")
	WebElement eleCompanyName;
	@And("Verify the Updated Company Name as (.*)")
	public ViewLeadPage verifyCompanyName(String data) {		
		verifyPartialText(eleCompanyName, data);
		return this;
	}
	
	@FindBy(how = How.XPATH, using = "//a[text()='Edit']")
	WebElement eleEditButton;
	@And("Click on Edit button")
	public EditLeadPage clickEditButton() {		
		click(eleEditButton);
		return new EditLeadPage();
	}
	
	
}
