package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods{

	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(how = How.XPATH, using = "(//img[@alt='Lookup'])[1]")
	WebElement eleFromLead;
	@And("Click ImageIcon in FromLead Textbox")
	public MergeFindLeadPage clickFromLead() {		
		click(eleFromLead);
		switchToWindow(1);
		return new MergeFindLeadPage();
	}
	
	@FindBy(how = How.XPATH, using = "(//img[@alt='Lookup'])[2]")
	WebElement eleToLead;
	@And("Click ImageIcon in ToLead Textbox")
	public MergeFindLeadPage clickToLead() {		
		click(eleToLead);
		switchToWindow(1);
		return new MergeFindLeadPage();
	}
	
	@FindBy(how = How.XPATH, using = "//a[text()='Merge']")
	WebElement eleMergeButton;
	@And("Click on MergeButton")
	public MergeLeadPage clickMerge() {		
		click(eleMergeButton);
		return this;
	}
	@And("Accept to Merge")
	public MergeLeadPage acceptToMerge() {
		acceptAlert();
		return this;
	}
	
		
	
}
