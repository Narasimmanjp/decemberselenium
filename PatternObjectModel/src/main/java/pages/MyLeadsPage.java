package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement eleCreateLead;
	@And("Click on CreateLead")
	public CreateLeadPage clickCreateLead() {		
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	@FindBy(how = How.LINK_TEXT, using = "Find Leads")
	WebElement eleFindLeads;
	@And("Click on FindLead Link")
	public FindLeadPage clickFindLeads() {		
		click(eleFindLeads);
		return new FindLeadPage();
	}
	
	@FindBy(how = How.LINK_TEXT, using = "Merge Leads")
	WebElement eleMergeLeads;
	@And("Click on MergeLead Link")
	public MergeLeadPage clickMergeLead() {		
		click(eleMergeLeads);
		return new MergeLeadPage();
	}
	

	
	
	
	
	
}
