Feature: Merge Lead in Leaftaps App
Scenario Outline: MergeLead with Valid Data
	And Enter the UserName as <UserName>
And Enter the PassWord as <Password>
And Click Login Button 
And Click Crmsfa
#Then Verify the LeadsPage
And Click on Leads Tab
And Click on MergeLead Link 
And Click ImageIcon in FromLead Textbox	
And Enter LeadId to Find as <Lead1FindbyID> for Lead1 
And Enter CompanyName to Find as <Lead1FindbyCompany> for Lead1
And Enter FirstName to Find as <Lead1FindbyFirstName> for Lead1	
And Enter LastName to Find as <Lead1FindbyLastName> for Lead1	
And Click Find Lead button 
And Click on FirstResultingLead		 

And Click ImageIcon in ToLead Textbox
And Enter LeadId to Find as <Lead2FindbyID> for Lead2
And Enter CompanyName to Find as <Lead2FindbyCompany> for Lead2		
And Enter FirstName to Find as <Lead2FindbyFirstName> for Lead2	
And Enter LastName to Find as <Lead2FindbyLastName> for Lead2	
And Click Find Lead button
And Click on FirstResultingLead		

And Click on MergeButton
And Accept to Merge
Examples:
|UserName|Password|Lead1FindbyID|Lead1FindbyCompany|Lead1FindbyFirstName|Lead1FindbyLastName|Lead2FindbyID|Lead2FindbyCompany|Lead2FindbyFirstName|Lead2FindbyLastName|
|DemoSalesManager|crmsfa||TestLeaf|a|||TestLead|b|||

