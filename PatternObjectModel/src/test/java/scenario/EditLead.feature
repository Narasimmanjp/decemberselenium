Feature: Create Lead in Leaftaps App

#Background:
#Given Open the Browser
#And maximize the Browser
#And Set the TimeOut
#And Launch the URL as http://leaftaps.com/opentaps/control/login

Scenario Outline: CreateLead with Valid Data
And Enter the UserName as <UserName>
And Enter the PassWord as <Password>
And Click Login Button 
And Click Crmsfa
#Then Verify the LeadsPage
And Click on Leads Tab
And Click on FindLead Link 
And Enter the LeadId as <findbyID>
And Enter the companyName as <findbyCompany> 
And Enter the firstName as <FirstName>
And Enter the lastName as <LastName> 
And Click Find Lead 
And Click First Resulting Lead
And Click on Edit button
And Enter the new Company Name as <NewCompanyName>
And Enter the new first Name as <NewFirstName>
And Enter the new lastName as <NewLastName>
And Click on Update Button
Then Verify the Updated Company Name as <NewCompanyName>
#Then Verify the Creation of Lead with FirstName


Examples:
|UserName|Password|findbyID|findbyCompany|FirstName|LastName|NewCompanyName|NewFirstName|NewLastName|
|DemoSalesManager|crmsfa||TestLeaf|||TL|Narasimman|JP|
#|DemoSalesManager|crmsfa|TL|Poorni|S|
