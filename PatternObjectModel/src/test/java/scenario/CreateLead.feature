Feature: Create Lead in Leaftaps App

#Background:
#Given Open the Browser
#And maximize the Browser
#And Set the TimeOut
#And Launch the URL as http://leaftaps.com/opentaps/control/login

Scenario Outline: CreateLead with Valid Data
And Enter the UserName as <UserName>
And Enter the PassWord as <Password>
And Click Login Button 
And Click Crmsfa
#Then Verify the LeadsPage
And Click on Leads Tab
And Click on CreateLead 
And Enter the FirstName as <FirstName>
And Enter the CompanyName as <CompanyName> 
And Enter the LastName as <LastName>
When Click on CreateButton
#Then Verify the Creation of Lead with FirstName


Examples:
|UserName|Password|CompanyName|FirstName|LastName|
|DemoSalesManager|crmsfa|TL|Narasimman|JP|
#|DemoSalesManager|crmsfa|TL|Poorni|S|


Scenario Outline: CreateLead with inValid Data
And Enter the UserName as <UserName>
And Enter the PassWord as <Password>
And Click Login Button
And Click Crmsfa
#Then Verify the LeadsPage
And Click on Leads Tab
And Click on CreateLead
And Enter the FirstName as <FirstName>
And Enter the CompanyName as <CompanyName> 
And Enter the LastName as <LastName>
When Click on CreateButton
#Then Verify the Creation of Lead with FirstName


Examples:
|UserName|Password|CompanyName|FirstName|LastName|
|DemoSalesManager|crmsfa||Narasimman|JP|
#|DemoSalesManager|crmsfa|Leaf||S|